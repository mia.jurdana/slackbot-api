require('dotenv').config()
const express = require("express")
const cors = require('cors')

const slackRouter = require('./slack');

const app = express()
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 3000
const { createEventAdapter } = require('@slack/events-api');
const slackEvents = createEventAdapter(process.env.SLACK_SIGNING_SECRET);
const { WebClient} = require("@slack/web-api");
const client = new WebClient(process.env.SLACK_BOT_TOKEN)

app.use('/', slackEvents.requestListener());

//app.use(express.urlencoded({ extended: true }));
//app.use(express.json());
//app.use(cors());


//ovo radi
const result =  client.chat.postMessage({
  // The token you used to initialize your app
  channel: "moonbot-test",
  text: "Ova poruka zaista radi OMG!"
  // You could also use a blocks[] array to send richer content
});


//ovo ne radi jer nisam rješila verifikaciju URL-a
slackEvents.on('message', (event) => {
  console.log(`Received a message event: user ${event.user} in channel ${event.channel} says ${event.text}`);
});


app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`)
})
